module.exports = function(grunt) {
  grunt.registerTask("buildApiTest", ['clean:apiTest', 'coffee:apiTest', 'coffee:apiTestHelpers', 'coffeelint', 'mochaTest:apiTest']);
};

//# sourceMappingURL=buildApiTest.js.map
