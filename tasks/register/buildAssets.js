module.exports = function(grunt) {
  grunt.registerTask("buildAssets", ["clean:dev", "buildYaml", "buildCoffee", "buildLibs", "buildLess", "buildJade", "concat:dev", "copy:dev"]);
};

//# sourceMappingURL=buildAssets.js.map
