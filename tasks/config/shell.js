
/**
Compile CoffeeScript files to JavaScript.

---------------------------------------------------------------

Compiles coffeeScript files from `assest/js` into Javascript and places them into
`.tmp/public/js` directory.

For usage docs see:
https://github.com/gruntjs/grunt-contrib-coffee
 */
module.exports = function(grunt) {
  grunt.config.set("shell", {
    startWebDriver: {
      command: "webdriver-manager start"
    }
  });
  grunt.loadNpmTasks("grunt-shell");
};

//# sourceMappingURL=shell.js.map
