module.exports = function(grunt) {
  grunt.config.set("mochaTest", {
    apiTest: {
      src: 'test/api/test/**/*.spec.js',
      options: {
        reporter: 'mocha-unfunk-reporter'
      }
    }
  });
  grunt.loadNpmTasks("grunt-mocha-test");
};

//# sourceMappingURL=mochaTest.js.map
